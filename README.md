![](https://i.postimg.cc/NGX6X6GP/image.png)

[中文版本](./README.md) | [English Version](./README-en.md)

# 某熊的技术之路指北 ☯

当我们站在技术之路的原点，未来可能充满了迷茫，也存在着很多不同的可能；我们可能成为 **Web/(大)前端/终端工程师、服务端架构工程师、测试/运维/安全工程师等质量保障、可用性保障相关的工程师、大数据/云计算/虚拟化工程师、算法工程师、产品经理**等等某个或者某几个角色。[某熊的技术之路](https://github.com/topics/wx-doc)系列文章/书籍/视频/代码即是笔者蹒跚行进于这条路上的点滴印记，包含了笔者作为程序员的技术视野、知识管理与职业规划，致力于提升开发者的学习能力与实际研发效能。

[本指北](https://github.com/wx-chevalier/Developer-Zero-To-Mastery)就是对笔者不同领域方面沉淀下的知识仓库的导航与索引，便于读者快速地寻找到自己需要的内容。路漫漫其修远兮，吾正上下而求索，也希望能给所有遇见过笔者痕迹的同学些许帮助，在浩瀚银河间能顺利达到一个又一个彼岸。Just Coder，Travel in Galaxy，欢迎关注[某熊的技术之路](https://i.postimg.cc/mDxdH0VL/image.png)公众号，让我们一起前行。

您可以通过以下任一方式阅读笔者的系列文章，涵盖了技术资料归纳、编程语言与理论、Web 与大前端、服务端开发与基础架构、云计算与大数据、数据科学与人工智能、产品设计等多个领域：

- 在 Gitbook 中在线浏览，每个系列对应各自的 Gitbook 仓库。

| [Awesome Lists](https://ngte-al.gitbook.io/i/) | [Awesome CheatSheets](https://ngte-ac.gitbook.io/i/) | [Awesome Interviews](https://github.com/wx-chevalier/Developer-Zero-To-Mastery/tree/master/Interview) | [Awesome RoadMaps](https://github.com/wx-chevalier/Developer-Zero-To-Mastery/tree/master/RoadMap) | [Awesome-CS-Books-Warehouse](https://github.com/wx-chevalier/Awesome-CS-Books-Warehouse) |
| ---------------------------------------------- | ---------------------------------------------------- | ----------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------- |


| [编程语言理论](https://ngte-pl.gitbook.io/i/) | [Java 实战](https://ngte-pl.gitbook.io/i/go/go) | [JavaScript 实战](https://ngte-pl.gitbook.io/i/javascript/javascript) | [Go 实战](https://ngte-pl.gitbook.io/i/go/go) | [Python 实战](https://ngte-pl.gitbook.io/i/python/python) | [Rust 实战](https://ngte-pl.gitbook.io/i/rust/rust) |
| --------------------------------------------- | ----------------------------------------------- | --------------------------------------------------------------------- | --------------------------------------------- | --------------------------------------------------------- | --------------------------------------------------- |


| [软件工程、数据结构与算法、设计模式、软件架构](https://ngte-se.gitbook.io/i/) | [现代 Web 开发基础与工程实践](https://ngte-web.gitbook.io/i/) | [大前端混合开发与数据可视化](https://ngte-fe.gitbook.io/i/) | [服务端开发实践与工程架构](https://ngte-be.gitbook.io/i/) | [分布式基础架构](https://ngte-infras.gitbook.io/i/) | [数据科学，人工智能与深度学习](https://ngte-aidl.gitbook.io/i/) | [产品设计与用户体验](https://ngte-pd.gitbook.io/i/) |
| ----------------------------------------------------------------------------- | ------------------------------------------------------------- | ----------------------------------------------------------- | --------------------------------------------------------- | --------------------------------------------------- | --------------------------------------------------------------- | --------------------------------------------------- |


- 前往 [xCompass https://wx-chevalier.github.io](https://wx-chevalier.github.io/home/#/search) 交互式地检索、查找需要的文章/链接/书籍/课程，或者关注微信公众号：某熊的技术之路。

![](https://i.postimg.cc/3RVYtbsv/image.png)

- 在下文的 [MATRIX 文章与代码矩阵 https://github.com/wx-chevalier/Developer-Zero-To-Mastery](https://github.com/wx-chevalier/Developer-Zero-To-Mastery) 中查看文章与项目的源代码。

# MATRIX | 文章与代码源矩阵

[![image.png](https://i.postimg.cc/y1QXgJ6f/image.png)](https://postimg.cc/bZFSQcfz)

- 知识检索、资料归档、实践清单

| [Developer-Zero-To-Mastery](https://github.com/wx-chevalier/Developer-Zero-To-Mastery) | [Awesome-Lists](https://github.com/wx-chevalier/Awesome-Lists) | [Awesome-CheatSheets](https://github.com/wx-chevalier/Awesome-CheatSheets) | [Awesome-CS-Books-Warehouse](https://github.com/wx-chevalier/Awesome-CS-Books-Warehouse) | [Awesome-Interviews](https://github.com/wx-chevalier/Developer-Zero-To-Mastery) |
| -------------------------------------------------------------------------------------- | -------------------------------------------------------------- | -------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------- |


| [xCompass](https://github.com/wx-chevalier/xCompass) |
| ---------------------------------------------------- |


- 编程语言理论与实践

| [ProgrammingLanguage Theory Primer/编程语言理论指南](https://github.com/wx-chevalier/ProgrammingLanguage-Series/blob/master/编程语言理论) | [JavaScript 篇](https://github.com/wx-chevalier/ProgrammingLanguage-Series/blob/master/JavaScript) | [Java 篇](https://github.com/wx-chevalier/ProgrammingLanguage-Series/blob/master/Java) | [Python 篇](https://github.com/wx-chevalier/ProgrammingLanguage-Series/blob/master/Python) | [Go 篇](https://github.com/wx-chevalier/ProgrammingLanguage-Series/blob/master/Go) | [Rust 篇](https://github.com/wx-chevalier/ProgrammingLanguage-Series/blob/master/Rust) |
| ----------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------- |


- 软件工程、数据结构与算法、设计模式、软件架构

| [数据结构与算法](https://github.com/wx-chevalier/SoftwareEngineering-Series/blob/master/数据结构与算法) | [面向对象的设计模式](https://github.com/wx-chevalier/SoftwareEngineering-Series/blob/master/面向对象的设计模式) | [整洁与重构](https://github.com/wx-chevalier/SoftwareEngineering-Series/blob/master/整洁与重构) | [软件架构设计](https://github.com/wx-chevalier/SoftwareEngineering-Series/blob/master/软件架构设计) | [研发方式与工具](https://github.com/wx-chevalier/SoftwareEngineering-Series/blob/master/研发方式与工具) |
| ------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------- |


| [algorithm-snippets](https://github.com/wx-chevalier/algorithm-snippets) | [design-pattern-snippets](https://github.com/wx-chevalier/design-pattern-snippets) |
| ------------------------------------------------------------------------ | ---------------------------------------------------------------------------------- |


- 现代 Web 开发基础与工程实践

| [基础篇](https://github.com/wx-chevalier/Web-Series/blob/master/基础) | [工程实践篇](https://github.com/wx-chevalier/Web-Series/blob/master/工程实践) | [架构优化篇](https://github.com/wx-chevalier/Web-Series/blob/master/架构优化篇) | [React 篇](https://github.com/wx-chevalier/Web-Series/blob/master/React) | [Vue 篇](https://github.com/wx-chevalier/Web-Series/blob/master/Vue) |
| --------------------------------------------------------------------- | ----------------------------------------------------------------------------- | ------------------------------------------------------------------------------- | ------------------------------------------------------------------------ | -------------------------------------------------------------------- |


- 大前端混合开发与数据可视化

| [iOS 实战篇](https://github.com/wx-chevalier/Frontend-Series/blob/master/iOS) | [Android 实战篇](https://github.com/wx-chevalier/Frontend-Series/blob/master/Android) | [Hybrid 混合开发篇](https://github.com/wx-chevalier/Frontend-Series/blob/master/Hybrid) | [数据可视化篇](https://github.com/wx-chevalier/Frontend-Series/blob/master/DataVisualization) | [IoT 实战篇](https://github.com/wx-chevalier/Frontend-Series/blob/master/IoT) |
| ----------------------------------------------------------------------------- | ------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------- |


- 服务端开发

| [服务端应用程序开发基础](https://github.com/wx-chevalier/Backend-Series/blob/master/服务端基础) | [微服务与云原生](https://github.com/wx-chevalier/Backend-Series/blob/master/微服务与云原生) | [深入浅出 Node.js 全栈架构](https://github.com/wx-chevalier/Backend-Series/blob/master/Node) | [Spring Boot 5 与 Spring Cloud 微服务实践](https://github.com/wx-chevalier/Backend-Series/blob/master/Spring) | [DevOps 与 SRE 实战](https://github.com/wx-chevalier/Backend-Series/blob/master/DevOps) | [信息安全与渗透测试必知必会](https://github.com/wx-chevalier/Backend-Series/blob/master/信息安全与渗透测试) | [测试与高可用保障](https://github.com/wx-chevalier/Backend-Series/blob/master/测试与高可用保障) |
| ----------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------- |


- 分布式基础架构

| [Linux 与操作系统篇](https://github.com/wx-chevalier/Distributed-Infrastructure-Series/blob/master/Linux%20与操作系统) | [分布式计算篇](https://github.com/wx-chevalier/Distributed-Infrastructure-Series/blob/master/分布式计算) | [虚拟化与编排篇](https://github.com/wx-chevalier/Distributed-Infrastructure-Series/blob/master/虚拟化与编排) | [分布式系统篇](https://github.com/wx-chevalier/Distributed-Infrastructure-Series/blob/master/分布式系统) | [数据库篇](https://github.com/wx-chevalier/Distributed-Infrastructure-Series/blob/master/数据库) |
| ---------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------ |


- 数据科学、人工智能、机器学习、深度学习、自然语言处理

| [数理统计篇](https://github.com/wx-chevalier/AIDL-Series/blob/master/数理统计) | [数据分析篇](https://github.com/wx-chevalier/AIDL-Series/blob/master/数据分析) | [机器学习篇](https://github.com/wx-chevalier/AIDL-Series/blob/master/机器学习) | [深度学习篇](https://github.com/wx-chevalier/AIDL-Series/blob/master/深度学习) | [自然语言处理篇](https://github.com/wx-chevalier/AIDL-Series/blob/master/自然语言处理) |
| ------------------------------------------------------------------------------ | ------------------------------------------------------------------------------ | ------------------------------------------------------------------------------ | ------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------- |


| [推荐系统等行业应用篇](https://github.com/wx-chevalier/AIDL-Series/blob/master/行业应用) | [课程笔记篇](https://github.com/wx-chevalier/AIDL-Series/blob/master/课程笔记) | [TensorFlow & PyTorch 等工具实践篇](https://github.com/wx-chevalier/AIDL-Series/blob/master/工具实践) |
| ---------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------- |


- 产品与设计

| [产品设计篇](https://github.com/wx-chevalier/Product-Series/blob/master/产品设计) | [交互体验篇](https://github.com/wx-chevalier/Product-Series/blob/master/交互体验) | [项目管理篇](https://github.com/wx-chevalier/Product-Series/blob/master/项目管理) | [文档处理篇](https://github.com/wx-chevalier/Product-Series/blob/master/文档处理) | [行业迷思篇](https://github.com/wx-chevalier/Product-Series/blob/master/行业迷思) | [智能制造篇](https://github.com/wx-chevalier/Product-Series/blob/master/智能制造) | [电子商务篇](https://github.com/wx-chevalier/Product-Series/blob/master/电子商务) |
| --------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- |


# Copyright | 版权

![](https://parg.co/bDY) ![](https://parg.co/bDm)

笔者所有文章遵循[知识共享 署名 - 非商业性使用 - 禁止演绎 4.0 国际许可协议](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.zh)，欢迎转载，尊重版权。如果觉得本系列对你有所帮助，欢迎给我家布丁买点狗粮(支付宝扫码)~

![](https://github.com/wx-chevalier/OSS/blob/master/2017/8/1/Buding.jpg?raw=true)
